# ParaView Translations 101

The translation of ParaView is a long process. This README aims to resume all merge requests and features this process adds to ParaView, so developpers digging for infos are not too lost.

## Related merge requests
Nearly all of my [ParaView MR](https://gitlab.kitware.com/paraview/paraview/-/merge_requests?scope=all&state=merged&assignee_username=nicolasB) are about translations.

### ParaView repository

* Refactors to make cpp string translatable: [the main one](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/5919) (+ [its hotfix](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/5933)), [Plugins and examples](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/5934), [XRInterface](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/5970), [more](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/5984) and [more fixes](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/5968)
* Refactors to make XML elements translatable: [GetXMLLabel()](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/5961)
* [CMake translation source files generation](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/5971)
* [Translation loading system and translation test plugin](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6005)
  * [Loading system improval](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6101)
  * [Ignore editable text in translatability testing](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6126)
* [Translation developer documentation](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6047)
* [CI Job to get the translations](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6109)
* [Improve translations](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6316)
* [Rework pretty labels](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6323)

### ParaView Superbuild

* [Superbuild integration to ship the translations](https://gitlab.kitware.com/paraview/paraview-superbuild/-/merge_requests/1047)

### ParaView translations

* [Add of the first template files](https://gitlab.kitware.com/paraview/paraview-translations/-/merge_requests/1)
* [Add of the CMake build system + its CI](https://gitlab.kitware.com/paraview/paraview-translations/-/merge_requests/4)
* [Add of `INSTALL_SUBDIR`](https://gitlab.kitware.com/paraview/paraview-translations/-/merge_requests/6/)
* [Add of qt translations as submodule](https://gitlab.kitware.com/paraview/paraview-translations/-/merge_requests/10)
* [REAMDE improval](https://gitlab.kitware.com/paraview/paraview-translations/-/merge_requests/8)

## Related issues

* Once [macros are ignored by `--dr`](https://gitlab.kitware.com/paraview/paraview/-/issues/21706), we will be able to do the test `pv.LoadTranslationsTestingPlugin`.
* Once [plugin EULA prompts are skipped by CTest](https://gitlab.kitware.com/paraview/paraview/-/issues/21691), [the testing of TanslationsTesting plugin](https://gitlab.kitware.com/paraview/paraview/-/tree/master/Plugins/TranslationsTesting/Testing) will have to be refactored to [use `REQUIRE_PLUGIN`](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/6005#note_1285306) instead of an XML manually loading the plugin.
* Some methods [uses Qt labels in code logic](https://gitlab.kitware.com/paraview/paraview/-/issues/21593).

## Features

* Translations source files generation ([this MR](https://gitlab.kitware.com/paraview/paraview/-/merge_requests/5971))
* Source files translation into other languages ([this repo](https://gitlab.kitware.com/paraview/paraview-translations))
* Generating translation binary files
* Shipping binary files into ParaView's translations shared directory
* Choosing UI language from `Interface language` section of the general settings.

## Related ParaView environment variables

* `PV_TRANSLATIONS_DIR`: Additional directories containing translations binary files.
* `PV_TRANSLATIONS_LOCALE`: Set the language to load (needed for testing purposes).

## Related ParaView CMake variables

* `PARAVIEW_BUILD_TRANSLATIONS` (default to `OFF`): Enable translation
  files update and generation.
* `PARAVIEW_TRANSLATIONS_DIRECTORY` (default to `${CMAKE_BINARY_DIR}/Translations`):
  Path where the translation files will be generated on build.
